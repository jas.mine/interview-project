Thank you for applying to Ringblaze.

## Usage

Install [nodemon](https://github.com/remy/nodemon) globally

```
npm i nodemon -g
```

Install `server` and `client` dependencies

```
npm i
cd server
npm i
cd ../client
npm i
```

To start the `server` and `client` at the same time, run at the root of project

```
npm start
```

To run tests, execute inside `server` or `client` folder

```
npm run test
```

## Tasks

1. Add an endpoint `api/users` to return all the users in json format by reading the data in `data` folder.
2. Replace the code that printing 'Hello from Express' on the homepage with a table showing all the users by fetching the data from the endpoint you added. Don't spend too much time on the UI. We just need a functional page.
3. Add unit tests to \*.test.js files to make you feel comfortable about the code.

---

- Make sure to commit all your changes using git.
- Install any additional packages you want to complete the tasks.

Good luck!
