import request from 'supertest';
import app from './server';

describe('server', () => {
    describe('/api/hello', () => {
        it('should response with text', () => {
            request(app)
            .get('/api/hello')
            .expect(200)
            .then(response => {
                expect(response.body.express).toBe('Hello From Express');
            })
        });
    });
});